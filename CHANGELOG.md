# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [Unreleased]

## [2020.07.1]

### Changed
- distribute with `-` instead of `_`

## [2020.07.0]

### Fixed
- prepended "fake" to continuous hardware protocol name, as intended

## [2020.06.1]

### Fixed
- include avpr and toml files in the distributed version

## [2020.06.0]

### Added
- initial release

[Unreleased]: https://gitlab.com/yaq/yaqd-fakes/-/compare/v2020.07.1...master
[2020.07.1]: https://gitlab.com/yaq/yaqd-fakes/-/compare/v2020.07.0...v2020.07.1
[2020.07.0]: https://gitlab.com/yaq/yaqd-fakes/-/compare/v2020.06.1...v2020.07.0
[2020.06.1]: https://gitlab.com/yaq/yaqd-fakes/-/compare/v2020.06.0...v2020.06.1
[2020.06.0]: https://gitlab.com/yaq/yaqd-fakes/-/tags/v2020.06.0
