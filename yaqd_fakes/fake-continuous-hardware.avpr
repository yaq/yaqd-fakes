{
    "config": {
        "limits": {
            "default": [
                0,
                1
            ],
            "doc": "Configuration limits are strictly optional.",
            "items": "float",
            "type": "array"
        },
        "make": {
            "default": null,
            "type": [
                "null",
                "string"
            ]
        },
        "model": {
            "default": null,
            "type": [
                "null",
                "string"
            ]
        },
        "out_of_limits": {
            "default": "closest",
            "doc": "Control behavior of daemon when set_position is given a value outside of limits.",
            "name": "out_of_limits",
            "symbols": [
                "closest",
                "ignore",
                "error"
            ],
            "type": "enum"
        },
        "port": {
            "doc": "TCP port for daemon to occupy.",
            "type": "int"
        },
        "serial": {
            "default": null,
            "doc": "Serial number for the particular device represented by the daemon",
            "type": [
                "null",
                "string"
            ]
        },
        "velocity": {
            "default": 1.0,
            "type": "float"
        }
    },
    "doc": "Idealized simulated hardware which immediately reports as the requested position upon setting.",
    "installation": {
        "PyPI": "https://pypi.org/projec/yaqd-core",
        "conda-forge": "https://anaconda.org/conda-forge/yaqd-core"
    },
    "links": {
        "bugtracker": "https://gitlab.com/yaq/yaqd-core-python/issues",
        "documentation": "https://yaqd-core-python.yaq.fyi/#yaqd_core.ContinuousHardware",
        "source": "https://gitlab.com/yaq/yaqd-core-python"
    },
    "messages": {
        "busy": {
            "doc": "Returns true if daemon is currently busy.",
            "request": [],
            "response": "boolean"
        },
        "get_config": {
            "doc": "Full configuration for the individual daemon as defined in the TOML file.\nThis includes defaults and shared settings not directly specified in the daemon-specific TOML table.\n",
            "request": [],
            "response": "string"
        },
        "get_config_filepath": {
            "doc": "String representing the absolute filepath of the configuration file on the host machine.\n",
            "request": [],
            "response": "string"
        },
        "get_destination": {
            "doc": "Get current daemon destination.",
            "request": [],
            "response": "float"
        },
        "get_limits": {
            "doc": "Get daemon limits.Limits will be the <a href='https://en.wikipedia.org/wiki/Intersection_(set_theory)'>intersection</a> of config limits and driver limits (when appliciable).",
            "request": [],
            "response": {
                "items": "float",
                "type": "array"
            }
        },
        "get_position": {
            "doc": "Get current daemon position.",
            "request": [],
            "response": "float"
        },
        "get_state": {
            "doc": "Get version of the running daemon",
            "request": [],
            "response": "string"
        },
        "get_units": {
            "doc": "Get units of daemon. These units apply to the position and destination fields.",
            "request": [],
            "response": [
                "null",
                "string"
            ]
        },
        "id": {
            "doc": "JSON object with information to identify the daemon, including name, kind, make, model, serial.\n",
            "request": [],
            "response": {
                "type": "map",
                "values": [
                    "null",
                    "string"
                ]
            }
        },
        "in_limits": {
            "doc": "Check if a given position is within daemon limits.",
            "request": [
                {
                    "name": "position",
                    "type": "float"
                }
            ],
            "response": "boolean"
        },
        "set_position": {
            "doc": "Give the daemon a new destination, and begin motion towards that destination.",
            "request": [
                {
                    "name": "position",
                    "type": "float"
                }
            ],
            "response": "null"
        },
        "set_relative": {
            "doc": "Give the daemon a new destination relative to its current position. Daemon will immediately begin motion towards new destination. Returns new destination.",
            "request": [
                {
                    "name": "distance",
                    "type": "float"
                }
            ],
            "response": "float"
        },
        "shutdown": {
            "doc": "Cleanly shutdown (or restart) daemon.",
            "request": [
                {
                    "default": false,
                    "name": "restart",
                    "type": "boolean"
                }
            ],
            "response": "null"
        }
    },
    "protocol": "fake-continuous-hardware",
    "requires": [],
    "state": {
        "destination": {
            "default": NaN,
            "type": "float"
        },
        "hw_limits": {
            "default": [
                -Infinity,
                Infinity
            ],
            "items": "float",
            "type": "array"
        },
        "position": {
            "default": NaN,
            "type": "float"
        }
    },
    "trait": "has-position",
    "traits": [
        "has-position",
        "has-limits",
        "is-daemon"
    ]
}
